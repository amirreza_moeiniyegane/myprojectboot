package com.codingtask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.codingtask.resourceaccess.ResourceManager;

@RestController
public class HomeController {

	@Autowired
	private ResourceManager resourceManager;

	@GetMapping("/")
	public ModelAndView homeView() {

		ModelAndView mav = new ModelAndView();

		String caesarCipherWiki = resourceManager.readText("caesar-cipher-wiki");
		String caesarCipherWikiHeader = resourceManager.readText("caesar-cipher-wiki-header");
		String caesarDiskCaption = resourceManager.readText("cipher-disk-caption");
		String handMadeCipherCaption = resourceManager.readText("hand-made-caesar-cipher-caption");
		String confederateCipherCaption = resourceManager.readText("confederate-cipher-disk-caption");
		String juliusCaesarCaption = resourceManager.readText("julius-caesar-caption");
		String juliusCaesarWiki = resourceManager.readText("julius-caesar-wiki");
		String juliusCaesarWikiHeader = resourceManager.readText("julius-caesar-wiki-header");
		String encryptSectionHeader = resourceManager.readText("encrypt-section-header");
		String encryptSectionSubHeader = resourceManager.readText("encrypt-section-subheader");
		String encryptionWiki = resourceManager.readText("encryption-wiki");
		String encryptionWikiHeader = resourceManager.readText("encryption-wiki-header");

		mav.addObject("caesarCipherWiki", caesarCipherWiki);
		mav.addObject("caesarCipherWikiHeader", caesarCipherWikiHeader);

		mav.addObject("caesarDiskCaption", caesarDiskCaption);

		mav.addObject("juliusCaesarCaption", juliusCaesarCaption);

		mav.addObject("handMadeCipherCaption", handMadeCipherCaption);

		mav.addObject("confederateCipherCaption", confederateCipherCaption);

		mav.addObject("juliusCaesarWiki", juliusCaesarWiki);
		mav.addObject("juliusCaesarWikiHeader", juliusCaesarWikiHeader);

		mav.addObject("encryptSectionHeader", encryptSectionHeader);
		mav.addObject("encryptSectionSubHeader", encryptSectionSubHeader);

		mav.addObject("encryptionWiki", encryptionWiki);
		mav.addObject("encryptionWikiHeader", encryptionWikiHeader);

		mav.setViewName("HomeView");

		return mav;
	}

}
