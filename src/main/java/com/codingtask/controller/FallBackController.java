package com.codingtask.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class FallBackController {

	@RequestMapping
	public ModelAndView fallBack(HttpServletRequest httpRequest) {
		ModelAndView mav = new ModelAndView();

		mav.setViewName("ErrorView");

		mav.addObject("errorHeader", HttpStatus.NOT_FOUND.value());
		mav.addObject("errorMessage",
				"The requested URL /" + httpRequest.getRequestURI() + " was not found on this server.");

		return mav;
	}

}
