package com.codingtask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingtask.exception.UrlNotFoundException;
import com.codingtask.resourceaccess.ResourceManager;

@RestController
@RequestMapping("/images")
public class ImageViewerController {

	@Autowired
	ResourceManager resourceManager;

	@RequestMapping("/{imageName}")
	public ResponseEntity<byte[]> viewImage(@PathVariable(name = "imageName") String imageName) {

		byte[] extractedImage = resourceManager.loadImage(imageName);

		if (extractedImage == null) {
			throw new UrlNotFoundException("The requested URL /" + imageName + " was not found on this server.");
		}

		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).contentType(MediaType.IMAGE_PNG)
				.body(extractedImage);
	}

}
