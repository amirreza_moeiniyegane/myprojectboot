package com.codingtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCodingTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyCodingTaskApplication.class, args);
	}

}
