package com.codingtask.exception;

public class EncryptionException extends RuntimeException {

	private static final long serialVersionUID = 983631694146299364L;

	public EncryptionException(String message) {
		super(message);
	}

}
