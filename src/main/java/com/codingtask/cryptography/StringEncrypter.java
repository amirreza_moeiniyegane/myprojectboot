package com.codingtask.cryptography;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

@Component("stringEncrypter")
public class StringEncrypter {

	/**
	 * English Alphabet.
	 */
	private static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
	/**
	 * A list of alphabet characters.
	 */
	private static ArrayList<Character> ALPHABET_LIST = new ArrayList<Character>();

	// initializing the list of alphabet characters.
	static {
		for (int i = 0; i < ALPHABET.length(); i++) {
			ALPHABET_LIST.add(ALPHABET.charAt(i));
		}
	}

	/**
	 * return the Caesar Cipher of the string shifts the script by the value of
	 * offset
	 * 
	 * @param Integer offset
	 * @param String  script
	 */
	public String encrypt(String script, int offset) {

		// if remainder of the offset divided by alphabet size (26) is zero,
		// then it doesn't affect the script
		if ((offset % ALPHABET_LIST.size()) == 0) {
			return script;
		}
		
		offset = (offset % ALPHABET_LIST.size());

		// an array booleans that tells if a particular character of the main
		// input(script) was an upper case.
		// the indexes of this array list is **synchronized with the indexes of
		// characters in the main input(script).**
		boolean[] isUpperCaseArray = new boolean[script.length()];

		// indicates which index was upper case.
		for (int i = 0; i < script.length(); i++) {
			isUpperCaseArray[i] = Character.isUpperCase(script.charAt(i));
		}

		// gets an array of characters from the script, turns everything to lower case.
		char[] charArray = script.toLowerCase().toCharArray();

		// loops through the array of characters.
		for (int i = 0; i < charArray.length; i++) {
			// skip this index if the character is not alphabetic, I.E, white spaces,
			// numbers, ETC.
			if (!Character.isAlphabetic(charArray[i])) {
				continue;
			}
			// gets the current character the loop.
			char currentChar = charArray[i];
			// find the index of current character in the alphabet list.
			int currentCharIndexInAlphabet = ALPHABET_LIST.indexOf(currentChar);
			// set the pointer to the index of current character in the alphabet list.
			int pointer = currentCharIndexInAlphabet;
			// records the movement of the pointer
			int pointerMovement = 0;

			// if the offset is greater than zero, it means we're shifting the characters
			// up,
			// otherwise we're shifting them down
			if (offset < 0) {
				// loops as much as the value of offset.
				while (pointerMovement > offset) {
					// moves the pointer, if it reaches the beginning of the alphabet, it goes to
					// the end.
					if (pointer - 1 >= 0) {
						pointer--;
					}

					else {
						pointer = ALPHABET_LIST.size() - 1;
					}

					pointerMovement--;

				} // loop ends here.

				// sets the character that pointer is pointing to the current character of the
				// char array.
				charArray[i] = ALPHABET_LIST.get(pointer);
			}

			// works exactly like the codes above but in other direction.
			else if (offset > 0) {
				while (pointerMovement < offset) {

					if (pointer + 1 <= ALPHABET_LIST.size() - 1) {
						pointer++;

					}

					else {
						pointer = 0;
					}

					pointerMovement++;

				}
				charArray[i] = ALPHABET_LIST.get(pointer);
			}

		}

		// searches through the array of upper cases to see which character of the main
		// input(script) was upper case
		// then turns it back to upper case.
		for (int i = 0; i < charArray.length; i++) {
			if (isUpperCaseArray[i]) {
				charArray[i] = Character.toUpperCase(charArray[i]);
			}
		}

		return new String(charArray);
	}
}
